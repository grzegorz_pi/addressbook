#ifndef DATASOURCE_H
#define DATASOURCE_H

#include <QObject>

#include "person.h"

class DataSource : public QObject
{
    Q_OBJECT

public:
    explicit DataSource(QObject *parent = nullptr);

    QList<Person *> dataItems();
    void addPerson(Person *person);
    void fetchPersons();


signals:
    void prePersonAdded();
    void postPersonAdded();

public slots:
    void dataParseFinished(QList<Person *> personsList);

private:
    QList<Person *> m_persons;
};

#endif // DATASOURCE_H
