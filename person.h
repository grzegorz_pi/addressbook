#ifndef PERSON_H
#define PERSON_H

#include <QObject>

class Person : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString firstName READ firstName WRITE setFirstName NOTIFY firstNameChanged)
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY lastNameChanged)
    Q_PROPERTY(int age READ age WRITE setAge NOTIFY ageChanged)
    Q_PROPERTY(QString homeNumber READ homeNumber WRITE setHomeNumber NOTIFY homeNumberChanged)
    Q_PROPERTY(QString officeNumber READ officeNumber WRITE setOfficeNumber NOTIFY officeNumberChanged)
    Q_PROPERTY(QString mobileNumber READ mobileNumber WRITE setMobileNumber NOTIFY mobileNumberChanged)

public:
    explicit Person(QObject *parent = nullptr);
    Person(const QString firstName, QString lastName, int age, QString homeNumber, QString officeNumber, QString mobileNumber, QObject * parent = nullptr);

    QString firstName() const;
    QString lastName() const;
    int age() const;
    QString homeNumber() const;
    QString officeNumber() const;
    QString mobileNumber() const;
    void read(const QJsonObject &json);

signals:
    void firstNameChanged(QString firstName);
    void lastNameChanged(QString lastName);
    void ageChanged(int age);
    void homeNumberChanged(QString homeNumber);
    void officeNumberChanged(QString officeNumber);
    void mobileNumberChanged(QString mobileNumber);

public slots:
    void setFirstName(QString firstName);
    void setLastName(QString lastName);
    void setAge(int age);
    void setHomeNumber(QString homeNumber);
    void setOfficeNumber(QString officeNumber);
    void setMobileNumber(QString mobileNumber);

private:
    QString m_firstName;
    QString m_lastName;
    int m_age;
    QString m_homeNumber;
    QString m_officeNumber;
    QString m_mobileNumber;
};

#endif // PERSON_H
