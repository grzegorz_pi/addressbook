#include "appwrapper.h"

#include <QQmlContext>

AppWrapper::AppWrapper(QObject *parent) : QObject(parent)
{

}

bool AppWrapper::initialize()
{
    DataSource * personsDataSource = new DataSource(&m_personModel);
    m_personModel.setDataSource(personsDataSource);
    personsDataSource->fetchPersons();

    m_engine.rootContext()->setContextProperty("personModel", &m_personModel);
    m_engine.rootContext()->setContextProperty("personDataSource", personsDataSource);
    m_engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (m_engine.rootObjects().isEmpty()){
        return false;
    } else {
        return true;
    }
}
