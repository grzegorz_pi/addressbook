#ifndef APPWRAPPER_H
#define APPWRAPPER_H

#include <QObject>
#include <QQmlApplicationEngine>

#include "datasource.h"
#include "personmodel.h"

class AppWrapper : public QObject
{
    Q_OBJECT

public:
    explicit AppWrapper(QObject *parent = nullptr);
    bool initialize();

private:
    QQmlApplicationEngine m_engine;
    PersonModel m_personModel;
};

#endif // APPWRAPPER_H
