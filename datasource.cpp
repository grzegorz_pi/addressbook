#include "datasource.h"

#include "fetchcontroller.h"

DataSource::DataSource(QObject *parent) : QObject(parent)
{

}

QList<Person *> DataSource::dataItems()
{
    return m_persons;
}

void DataSource::addPerson(Person *person)
{
    emit prePersonAdded();
    m_persons.append(person);
    emit postPersonAdded();
}

void DataSource::fetchPersons()
{
    FetchController * controller = new FetchController(this);
    connect(controller, SIGNAL(dataParseFinished(const QList<Person *> &)), this, SLOT(dataParseFinished(const QList<Person *> &)));
    controller->startFetching();
}

void DataSource::dataParseFinished(QList<Person *> personsList)
{
    foreach (Person * person, personsList) {
        addPerson(person);
    }
}
