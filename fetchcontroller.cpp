#include "fetchcontroller.h"

#include "jsonfilereadworker.h"

FetchController::FetchController(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QList<Person *>>("QList<Person *>");

    QString path("../AddressBook/");
    QString fileName("addressBook.json");

    JsonFileReadWorker *worker = new JsonFileReadWorker;
    worker->setFileName(fileName);
    worker->setPath(path);
    worker->moveToThread(&workerThread);

    connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
    connect(this, SIGNAL(loadAddressBook()), worker, SLOT(loadAddressBook()));
    connect(worker, SIGNAL(dataParseFinished(QList<Person *>)), parent, SLOT(dataParseFinished(QList<Person *>)));
    workerThread.start();
}

FetchController::~FetchController() {
    workerThread.quit();
    workerThread.wait();
}

void FetchController::startFetching()
{
    emit loadAddressBook();
}
