import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12

Page {
    width : Window.width
    height : Window.height

    title: qsTr("Address Book")

    Rectangle {
        id: backgroundPlan
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 1.0; color: "#1a2033" }
            GradientStop { position: 0.0; color: "#3a4055" }
        }

        ColumnLayout {
            anchors.fill : parent

            ListView {
                id : personListView
                model: personModel
                delegate: personDelegate

                Layout.fillWidth : true
                Layout.fillHeight: true
            }
        }
    }

    Component {
        id: personDelegate

        Rectangle {
            id: buttonBackground
            width: parent.width
            height: buttonRightSide.height

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#2CB2F3" }
                GradientStop { position: 0.64; color: "#3E65FF" }
                GradientStop { position: 0.65; color: "#AA00FF" }
                GradientStop { position: 1.0; color: "#6A00FF" }
            }
            radius: 5

            RowLayout {
                id: buttonLeftSide
                width: parent.width
                height: buttonRightSide.height
                spacing: 10

                Item {
                    id: positionId
                    height: buttonRightSide.height
                    width: buttonRightSide.height

                    Rectangle {
                        id: numberCircleBackground
                        height: buttonRightSide.height / 2
                        width: buttonRightSide.height / 2
                        anchors.centerIn: parent
                        border.color: "#5B00FF"
                        radius: 100

                        Text {
                            anchors.centerIn: parent
                            font { pixelSize: Qt.application.font.pixelSize * 1.6; bold: true }
                            color: "#6A00FF"
                            text: index+1
                        }
                    }
                }

                Column {
                    id: buttonRightSide
                    width: parent.width
                    height: nameId.height * 3 + 30
                    padding: 12
                    spacing: 3

                    Text {
                        id: nameId
                        color: "yellow"
                        font.bold: true
                        text: "name: " + model.firstName
                    }
                    Text {
                        color: "yellow"
                        font.bold: true
                        text: "surname: " + model.lastName
                    }
                    Text {
                        color: "beige"
                        font.bold: true
                        text: "home no: " + model.homeNumber
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    stackView.push("qrc:/personDetailsView.qml",
                                   {
                                       personDetails : "name: " + model.firstName
                                                        + "\nsurname: " + model.lastName
                                                        + "\nage: " + model.age
                                       , phoneNumbers : "home no: " + model.homeNumber
                                                        + "\noffice no: " + model.officeNumber
                                                        + "\nmobile no: " + model.mobileNumber
                                   })
                }
            }
        }
    }
}

