#ifndef JSONFILEREADWORKER_H
#define JSONFILEREADWORKER_H

#include <QJsonObject>
#include <QObject>

#include "person.h"

class JsonFileReadWorker : public QObject
{
    Q_OBJECT

public:
    explicit JsonFileReadWorker(QObject *parent = nullptr);
    void setFileName(const QString &fileName);
    void setPath(const QString &path);

signals:
    void dataParseFinished(QList<Person *>);

public slots:
    void loadAddressBook();

private:
    void parseJson(const QJsonObject &json);
    Person * readPersonDetails(const QJsonObject &json);

private:
    QString m_fileName;
    QString m_path;
};

#endif // JSONFILEREADWORKER_H
