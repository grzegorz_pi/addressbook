import QtQuick 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    id: window
    visible: true

    width: 272
    height: 480

    StackView {
        id: stackView

        initialItem: "adressBookView.qml"
        anchors.fill: parent
    }
}
