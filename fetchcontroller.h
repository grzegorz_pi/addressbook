#ifndef FETCHCONTROLLER_H
#define FETCHCONTROLLER_H

#include <QObject>
#include <QThread>

#include "person.h"

class FetchController : public QObject
{
    Q_OBJECT
    QThread workerThread;

public:
    explicit FetchController(QObject *parent = nullptr);
    ~FetchController();

    void startFetching();

signals:
    void loadAddressBook();
    void dataParseFinished(const QList<Person *> &);
};

#endif // FETCHCONTROLLER_H
