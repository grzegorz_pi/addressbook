#include "jsonfilereadworker.h"

#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>

JsonFileReadWorker::JsonFileReadWorker(QObject *parent)
    : QObject(parent)
    , m_fileName(QString())
    , m_path(QString())
{

}

void JsonFileReadWorker::setFileName(const QString &fileName)
{
    m_fileName = fileName;
}

void JsonFileReadWorker::setPath(const QString &path)
{
    m_path = path;
}

void JsonFileReadWorker::loadAddressBook()
{
    QFile loadFile(m_path+m_fileName);

    if (!loadFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray dataArray = loadFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(dataArray));
    parseJson(loadDoc.object());
}

void JsonFileReadWorker::parseJson(const QJsonObject &json)
{
    QList<Person *> personsList;

    if (json.contains("addressBook") && json["addressBook"].isArray()) {
        QJsonArray addressBook = json["addressBook"].toArray();

        for (int i = 0; i < addressBook.size(); ++i) {
            QJsonObject personObject = addressBook[i].toObject();
            personsList.append(readPersonDetails(personObject));
        }
    }

    emit dataParseFinished(personsList);
}

Person * JsonFileReadWorker::readPersonDetails(const QJsonObject &json)
{
    Person * person = new Person();

    if (json.contains("firstName") && json["firstName"].isString()){
        person->setFirstName(json["firstName"].toString());
    }

    if (json.contains("lastName") && json["lastName"].isString()){
        person->setLastName(json["lastName"].toString());
    }

    if (json.contains("age") && json["age"].isDouble()){
        person->setAge(json["age"].toInt());
    }

    if (json.contains("phoneNumbers") && json["phoneNumbers"].isArray()) {
        QJsonArray phoneNumbers = json["phoneNumbers"].toArray();

        for (int i = 0; i < phoneNumbers.size(); ++i){
            QJsonObject phoneNumber = phoneNumbers[i].toObject();
            QString phoneType;

            if (phoneNumber.contains("type") && phoneNumber["type"].isString()){
                phoneType = phoneNumber["type"].toString();
            }

            if (phoneNumber.contains("number") && phoneNumber["number"].isString()){
                if (phoneType == "home"){
                    person->setHomeNumber(phoneNumber["number"].toString());

                } else if (phoneType == "office"){
                    person->setOfficeNumber(phoneNumber["number"].toString());

                } else if (phoneType == "mobile"){
                    person->setMobileNumber(phoneNumber["number"].toString());
                }
            }
        }
    }

    return person;
}
