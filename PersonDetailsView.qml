import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12


Page {
    width: Window.width
    height: Window.height

    title: qsTr("Person Details")

    property alias personDetails : personDetailsId.text
    property alias phoneNumbers : phoneNumbersId.text

    property var fontSize : Qt.application.font.pixelSize * 1.6
    property var fontcolor : "#EEEEEE"

    header: ToolBar {
        id: toolBarId
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: "\u25C0"
            font.pixelSize: fontSize
            onClicked: {
                stackView.pop()
            }
        }
    }

    Rectangle {
        anchors.fill : parent
        gradient: Gradient {
            GradientStop { position: 1.0; color: "#1a2033" }
            GradientStop { position: 0.3001; color: "#1a2033" }
            GradientStop { position: 0.3; color: "#2B3144" }
            GradientStop { position: 0.0; color: "#4C5366" }
        }

        ColumnLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 25

            Column {
                spacing: 10

                Text {
                    id: labelId
                    color: fontcolor
                    font {
                        underline : true;
                        pixelSize: fontSize;
                    }
                    text : qsTr("\nId:")
                }
                Text {
                    id: personDetailsId
                    color: fontcolor
                    font.pixelSize: fontSize
                    text: qsTr("Person data")
                }
            }

            Column {
                spacing: 10

                Text {
                    id: labelPhones
                    color: fontcolor
                    font {
                        underline : true;
                        pixelSize: fontSize
                    }
                    text: qsTr("Phones:")
                }
                Text {
                    id: phoneNumbersId
                    color: fontcolor
                    font.pixelSize: fontSize
                    text: qsTr("numbers")
                }
            }
        }
    }
}
