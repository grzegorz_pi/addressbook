#include "personmodel.h"

PersonModel::PersonModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_signalsConnected(false)
{
    setDataSource(new DataSource(this));
}

int PersonModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_dataSource->dataItems().size();
}

QVariant PersonModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_dataSource->dataItems().size()){
        return QVariant();

    } else {
        Person * person = m_dataSource->dataItems().at(index.row());

        if(role == FirstNameRole){
            return person->firstName();

        } else if (role == LastNameRole){
            return person->lastName();

        } else if (role == AgeRole){
            return person->age();

        } else if (role == HomeNumberRole){
            return person->homeNumber();

        } else if (role == OfficeNumberRole){
            return person->officeNumber();

        } else if (role == MobileNumberRole){
            return person->mobileNumber();
        }
    }

    return QVariant();
}

bool PersonModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    bool isChanged = false;
    Person * person = m_dataSource->dataItems().at(index.row());

    switch (role) {
    case FirstNameRole:
    {
        if (person->firstName() != value.toString()){
            person->setFirstName(value.toString());
            isChanged = true;
        }
    }
        break;
    case LastNameRole:
    {
        if (person->lastName() != value.toString()){
            person->setLastName(value.toString());
            isChanged = true;
        }
    }
        break;
    case AgeRole:
    {
        if (person->age() != value.toInt()){
            person->setAge(value.toInt());
            isChanged = true;
        }
    }
        break;
    case HomeNumberRole:
    {
        if (person->homeNumber() != value.toString()){
            person->setHomeNumber(value.toString());
            isChanged = true;
        }
    }
        break;
    case OfficeNumberRole:
    {
        if (person->officeNumber() != value.toString()){
            person->setOfficeNumber(value.toString());
            isChanged = true;
        }
    }
        break;
    case MobileNumberRole:
    {
        if (person->mobileNumber() != value.toString()){
            person->setMobileNumber(value.toString());
            isChanged = true;
        }
    }
        break;
    }

    if (isChanged){
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }

    return false;
}

Qt::ItemFlags PersonModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()){
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> PersonModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[FirstNameRole] = "firstName";
    roles[LastNameRole] = "lastName";
    roles[AgeRole] = "age";
    roles[HomeNumberRole] = "homeNumber";
    roles[OfficeNumberRole] = "officeNumber";
    roles[MobileNumberRole] = "mobileNumber";

    return roles;
}

void PersonModel::setDataSource(DataSource *dataSource)
{
    beginResetModel();
    if (m_dataSource && m_signalsConnected){
        m_dataSource->disconnect(this);
        m_signalsConnected = false;
    }

    m_dataSource = dataSource;

    connect(m_dataSource, &DataSource::prePersonAdded, this, [=](){
        const int index = m_dataSource->dataItems().size();
        beginInsertRows(QModelIndex(), index, index);
    });

    connect(m_dataSource, &DataSource::postPersonAdded, this, [=](){
        endInsertRows();
    });

    m_signalsConnected = true;

    endResetModel();
}

DataSource *PersonModel::dataSource() const
{
    return m_dataSource;
}
