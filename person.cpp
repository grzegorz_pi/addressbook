#include "person.h"

Person::Person(QObject *parent) : QObject(parent)
{

}

Person::Person(const QString firstName, QString lastName, int age, QString homeNumber, QString officeNumber, QString mobileNumber, QObject *parent)
    : QObject(parent)
    , m_firstName(firstName)
    , m_lastName(lastName)
    , m_age(age)
    , m_homeNumber(homeNumber)
    , m_officeNumber(officeNumber)
    , m_mobileNumber(mobileNumber)
{

}

QString Person::firstName() const
{
    return m_firstName;
}

QString Person::lastName() const
{
    return m_lastName;
}

int Person::age() const
{
    return m_age;
}

QString Person::homeNumber() const
{
    return m_homeNumber;
}

QString Person::officeNumber() const
{
    return m_officeNumber;
}

QString Person::mobileNumber() const
{
    return m_mobileNumber;
}

void Person::setFirstName(QString firstName)
{
    if (m_firstName == firstName)
        return;

    m_firstName = firstName;
    emit firstNameChanged(m_firstName);
}

void Person::setLastName(QString lastName)
{
    if (m_lastName == lastName)
        return;

    m_lastName = lastName;
    emit lastNameChanged(m_lastName);
}

void Person::setAge(int age)
{
    if (m_age == age)
        return;

    m_age = age;
    emit ageChanged(m_age);
}

void Person::setHomeNumber(QString homeNumber)
{
    if (m_homeNumber == homeNumber)
        return;

    m_homeNumber = homeNumber;
    emit homeNumberChanged(m_homeNumber);
}

void Person::setOfficeNumber(QString officeNumber)
{
    if (m_officeNumber == officeNumber)
        return;

    m_officeNumber = officeNumber;
    emit officeNumberChanged(m_officeNumber);
}

void Person::setMobileNumber(QString mobileNumber)
{
    if (m_mobileNumber == mobileNumber)
        return;

    m_mobileNumber = mobileNumber;
    emit mobileNumberChanged(m_mobileNumber);
}
